// Create a new module
var lifeApp = angular.module('lifeApp', ['ngResource']);

lifeApp.controller('homeCtrl', ['$scope', 'gameService', function ($scope, gameService) {
    $scope.name = 'world';
    $scope.isLoading = true;

    $scope.width = 400;
    $scope.height = 400;

    $scope.gridWidth = 10; // default val, will be overridden by API call
    $scope.gridHeight = 10; // default val, will be overridden by API call

    // init
    gameService.get({ id: 1 }, function (game) {
        console.log("game loaded", game);
        $scope.game = game;
        $scope.isLoading = false;
        $scope.gridWidth = $scope.game.orthogonalGrid.width;
        $scope.gridHeight = $scope.game.orthogonalGrid.height;
        renderGrid();
    });

    function getGrid() {
        let grid = document.getElementById('grid');
        if (grid.getContext) return grid.getContext('2d');
    }

    function clearGrid() {
        let grid = getGrid();
        grid.fillStyle = 'red';
        grid.clearRect(0, 0, $scope.width, $scope.height);
    }

    function renderGrid() {
        let grid = getGrid();
        clearGrid();
        //drawCell(grid, $scope.game.orthogonalGrid.columns[0].cells[5]);
        $scope.game.orthogonalGrid.columns.forEach(col => col.cells.forEach(c => drawCell(grid, c)));
    }

    function drawCell(grid, cell) {
        console.log("drawing cell", cell);
        let cellWidth = $scope.width / $scope.gridWidth;
        let cellHeight = $scope.height / $scope.gridHeight;
        let x = cell.coordinates.x * cellWidth;
        let y = cell.coordinates.y * cellHeight;
        grid.fillStyle = cell.isAlive ? "white" : "black";
        console.log("rendering", x, y, cellWidth, cellHeight, grid.fillStyle);
        grid.fillRect(x, y, cellWidth, cellHeight);
    }
}]);

lifeApp.factory('gameService', ['$resource', function ($resource) {
    return $resource('/api/games/:id');
}]);
﻿using Domain;
using Life.Services;
using System.Web.Http;

namespace Life.Web.Controllers
{
    [RoutePrefix("api/games")]
    public class GameApiController : ApiController
    {
        public GameService GameService { get; }

        public GameApiController(GameService gameService) => GameService = gameService;

        // GET api/<controller>
        [HttpGet, Route("{id}")]
        public Game Get(int id) => GameService.GetById(id);
    }
}
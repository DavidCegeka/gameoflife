namespace Life.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cells",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        X = c.Int(nullable: false),
                        Y = c.Int(nullable: false),
                        IsAlive = c.Boolean(nullable: false),
                        Column_Id = c.Int(),
                        Grid_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Columns", t => t.Column_Id)
                .ForeignKey("dbo.OrthogonalGrids", t => t.Grid_Id)
                .Index(t => t.Column_Id)
                .Index(t => t.Grid_Id);
            
            CreateTable(
                "dbo.OrthogonalGrids",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Games", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Columns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ColumnIndex = c.Int(nullable: false),
                        OrthogonalGrid_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrthogonalGrids", t => t.OrthogonalGrid_Id)
                .Index(t => t.OrthogonalGrid_Id);
            
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Seed = c.Short(nullable: false),
                        Generation = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrthogonalGrids", "Id", "dbo.Games");
            DropForeignKey("dbo.Cells", "Grid_Id", "dbo.OrthogonalGrids");
            DropForeignKey("dbo.Columns", "OrthogonalGrid_Id", "dbo.OrthogonalGrids");
            DropForeignKey("dbo.Cells", "Column_Id", "dbo.Columns");
            DropIndex("dbo.Columns", new[] { "OrthogonalGrid_Id" });
            DropIndex("dbo.OrthogonalGrids", new[] { "Id" });
            DropIndex("dbo.Cells", new[] { "Grid_Id" });
            DropIndex("dbo.Cells", new[] { "Column_Id" });
            DropTable("dbo.Games");
            DropTable("dbo.Columns");
            DropTable("dbo.OrthogonalGrids");
            DropTable("dbo.Cells");
        }
    }
}

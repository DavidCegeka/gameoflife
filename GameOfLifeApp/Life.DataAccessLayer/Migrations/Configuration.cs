namespace Life.DataAccessLayer.Migrations
{
    using Domain;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Life.DataAccessLayer.Config.LifeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Life.DataAccessLayer.Config.LifeContext context)
        {
            //  This method will be called after migrating to the latest version.

            context.Games.Any();
            if (!context.Games.AnyAsync().Result)
            {
                var baseGame = new Game(143, 10, 10);
                context.Games.AddOrUpdate(baseGame);
                //context.Grids.AddOrUpdate(baseGame.OrthogonalGrid);
                //baseGame.OrthogonalGrid.Columns.ToList().ForEach(x => context.Columns.AddOrUpdate(x));
                //baseGame.OrthogonalGrid.Columns.ToList().ForEach(x => x.Cells.ToList().ForEach(y => context.Cells.AddOrUpdate(y)));
            }
        }
    }
}

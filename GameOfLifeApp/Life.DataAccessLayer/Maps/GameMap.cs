﻿using Domain;
using System.Data.Entity.ModelConfiguration;

namespace Life.DataAccessLayer.Maps
{
    public class GameMap : EntityTypeConfiguration<Game>
    {
        public GameMap()
        {
            HasOptional(x => x.OrthogonalGrid)
                .WithRequired();
        }
    }
}

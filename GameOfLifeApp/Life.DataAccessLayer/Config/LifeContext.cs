﻿using Domain;
using System.Data.Entity;
using System.Reflection;
using static Domain.OrthogonalGrid;

namespace Life.DataAccessLayer.Config
{
    public class LifeContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<OrthogonalGrid> Grids { get; set; }
        public DbSet<Column> Columns { get; set; }
        public DbSet<Cell> Cells { get; set; }

        public LifeContext() : base("default") {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<LifeContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(GetType())); //Current Assembly
            base.OnModelCreating(modelBuilder);
        }
    }
}
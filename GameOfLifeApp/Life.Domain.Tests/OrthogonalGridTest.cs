using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DomainTests
{
    [TestClass]
    public class OrthogonalGridTest
    {
        [TestMethod]
        public void OrthogonalCtor_For2X2_PopulatesAllCellsAsDead()
        {
            var grid = new OrthogonalGrid(2, 2);
            foreach(var col in grid.Columns)
            {
                foreach(var cell in col.Cells)
                {
                    Assert.IsNotNull(cell, $"Oops! We have a dead spot in our grid!");
                    Assert.IsFalse(cell.IsAlive, $"Ohuh, we got a live one at ({cell.Coordinates.X}, {cell.Coordinates.Y})");
                }
            }
        }

        [TestMethod]
        public void OrthogonalCtor_For2X2_SetsCoordinatesCorrectly()
        {
            var grid = new OrthogonalGrid(2, 2);
            for (var col = 0; col < 2; col++)
            {
                for (var cell = 0; cell < 2; cell++)
                {
                    var cellCoords = grid[col][cell].Coordinates;
                    Assert.AreEqual(cellCoords.X, col, $"Oops! Expected x coordinate to be '{col}' but instead found '{cellCoords.X}'!");
                    Assert.AreEqual(cellCoords.Y, cell, $"Oops! Expected y coordinate to be '{cell}' but instead found '{cellCoords.Y}'!");
                }
            }
        }
    }
}

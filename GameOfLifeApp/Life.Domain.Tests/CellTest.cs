using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DomainTests
{
    [TestClass]
    public class CellTest
    {
        [TestMethod]
        public void LeftNeighbour_WithLeftNeighbours_ReturnsLeftNeighbour()
        {
            var grid = new OrthogonalGrid(2, 2);
            var leftCell = grid[1][0].LeftNeighbour;
            Assert.IsNotNull(leftCell, "Left cell was not returned!");
        }

        [TestMethod]
        public void LeftNeighbour_WithLeftNeighbours_ReturnsNull()
        {
            var grid = new OrthogonalGrid(2, 2);
            var leftCell = grid[0][0].LeftNeighbour;
            Assert.IsNull(leftCell, "Left cell returns incorrect cell!");
        }

        [TestMethod]
        public void RightNeighbour_WithRightNeighbours_ReturnsRightNeighbour()
        {
            var grid = new OrthogonalGrid(2, 2);
            var rightCell = grid[0][0].RightNeighbour;
            Assert.IsNotNull(rightCell, "Right cell was not returned!");
        }

        [TestMethod]
        public void RightNeighbour_WithRightNeighbours_ReturnsNull()
        {
            var grid = new OrthogonalGrid(2, 2);
            var rightCell = grid[1][0].RightNeighbour;
            Assert.IsNull(rightCell, "Right cell returns incorrect cell!");
        }

        [TestMethod]
        public void AboveNeighbour_WithAboveNeighbours_ReturnsRightNeighbour()
        {
            var grid = new OrthogonalGrid(2, 2);
            var aboveCell = grid[0][1].AboveNeighbour;
            Assert.IsNotNull(aboveCell, "Above cell was not returned!");
        }

        [TestMethod]
        public void AboveNeighbour_WithAboveNeighbours_ReturnsNull()
        {
            var grid = new OrthogonalGrid(2, 2);
            var aboveCell = grid[0][0].AboveNeighbour;
            Assert.IsNull(aboveCell, "Above cell returns incorrect cell!");
        }

        [TestMethod]
        public void BelowNeighbour_WithBelowNeighbours_ReturnsRightNeighbour()
        {
            var grid = new OrthogonalGrid(2, 2);
            var aboveCell = grid[0][0].BelowNeighbour;
            Assert.IsNotNull(aboveCell, "Below cell was not returned!");
        }

        [TestMethod]
        public void BelowNeighbour_WithBelowNeighbours_ReturnsNull()
        {
            var grid = new OrthogonalGrid(2, 2);
            var aboveCell = grid[0][1].BelowNeighbour;
            Assert.IsNull(aboveCell, "Below cell returns incorrect cell!");
        }

        [TestMethod]
        public void Neighbours_With3Neighbours_Returns3Neighbours()
        {
            var grid = new OrthogonalGrid(2, 2);
            var cells = grid[0][1].NeighbouringCells;
            Assert.IsNotNull(cells, "No cells returned!");
            Assert.AreEqual(cells.Count(), 3, $"Expected 3 neighbours but instead got {cells.Count()}!");
        }
    }
}

﻿using Domain;
using Life.DataAccessLayer.Config;
using System.Linq;
using System.Data.Entity;

namespace Life.Services
{
    public class GameService
    {
        public LifeContext Context { get; }

        public GameService(LifeContext context)
        {
            Context = context;
        }

        public Game GetById(int id)
        {
            var game = Context.Games.Include(x => x.OrthogonalGrid.Columns.Select(y => y.Cells))
               .SingleOrDefault(x => x.Id == id);
            game.OrthogonalGrid.Columns.ForEach(x => x.Cells.ForEach(y => y.Grid = game.OrthogonalGrid));
            return game;
        }
    }
}

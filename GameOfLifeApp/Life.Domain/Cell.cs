﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Cell : Entity
    {
        [JsonIgnore]
        public OrthogonalGrid Grid { get; set; }
        public CarthesianCoordinates Coordinates { get; set; }
        [JsonIgnore]
        public int X { get => Coordinates.X; set => Coordinates = new CarthesianCoordinates(value, Coordinates.Y); }
        [JsonIgnore]
        public int Y { get => Coordinates.Y; set => Coordinates = new CarthesianCoordinates(Coordinates.X, value); }
        public bool IsAlive { get; set; }

        public bool HasNeighbouringCellsOnLeft => Coordinates.X > 0;
        public bool HasNeighbouringCellsAbove => Coordinates.Y > 0;
        public bool HasNeighbouringCellsOnRight => (Coordinates.X+1) < Grid?.Width;
        public bool HasNeighbouringCellsBelow => (Coordinates.Y+1) < Grid?.Height;

        [JsonIgnore]
        public Cell UpperLeftCell => Grid != null && HasNeighbouringCellsOnLeft && HasNeighbouringCellsAbove ? Grid[Coordinates.X-1][Coordinates.Y-1] : null;
        [JsonIgnore]
        public Cell LeftNeighbour => Grid != null && HasNeighbouringCellsOnLeft ? Grid[Coordinates.X-1][Coordinates.Y] : null;
        [JsonIgnore]
        public Cell LowerLeftCell => Grid != null && HasNeighbouringCellsOnLeft && HasNeighbouringCellsBelow ? Grid[Coordinates.X-1][Coordinates.Y+1] : null;
        [JsonIgnore]
        public Cell AboveNeighbour => Grid != null && HasNeighbouringCellsAbove ? Grid[Coordinates.X][Coordinates.Y-1] : null;
        [JsonIgnore]
        public Cell BelowNeighbour => Grid != null && HasNeighbouringCellsBelow ? Grid[Coordinates.X][Coordinates.Y+1] : null;
        [JsonIgnore]
        public Cell UpperRightCell => Grid != null && HasNeighbouringCellsOnRight && HasNeighbouringCellsAbove ? Grid[Coordinates.X + 1][Coordinates.Y - 1] : null;
        [JsonIgnore]
        public Cell RightNeighbour => Grid != null && HasNeighbouringCellsOnRight ? Grid[Coordinates.X + 1][Coordinates.Y] : null;
        [JsonIgnore]
        public Cell LowerRightCell => Grid != null && HasNeighbouringCellsOnRight && HasNeighbouringCellsBelow ? Grid[Coordinates.X + 1][Coordinates.Y + 1] : null;

        [JsonIgnore]
        public IEnumerable<Cell> NeighbouringCells {
            get =>
                new List<Cell>() { 
                    UpperLeftCell, AboveNeighbour, UpperRightCell, 
                    LeftNeighbour,                 RightNeighbour,
                    LowerLeftCell, BelowNeighbour, LowerRightCell}
                .Where(x => x != null)
                .ToList();
        }

        public Cell() { /* EFCore <3 */}
        public Cell(OrthogonalGrid grid, int x, int y, bool isAlive = false)
        {
            Coordinates = new CarthesianCoordinates(x, y);
            IsAlive = isAlive;
            Grid = grid;
        }
        public Cell(OrthogonalGrid grid, uint x, int y, bool isAlive = false) : this(grid, (int)x, y, isAlive)
        {
            if (x < 0) throw new ArgumentException($"The passed X-coordinate '{x}' cannot be pessimistic!");
        }
        public Cell(OrthogonalGrid grid, uint x, uint y, bool isAlive = false) : this(grid, (int)x, (int)y, isAlive)
        {
            if (y < 0) throw new ArgumentException($"the passed Y-coordinate '{y}' cannot be pessimistic!");
        }

        public Cell IsBorn(OrthogonalGrid grid) => new Cell(grid, Coordinates.X, Coordinates.Y, true);
        public Cell Dies(OrthogonalGrid grid) => new Cell(grid, Coordinates.X, Coordinates.Y, false);
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class OrthogonalGrid : Entity
    {
        public int Width { get; set; }
        public int Height { get; set; }
        
        public List<Column> Columns { get; set; }

        public OrthogonalGrid() { /* EFCore <3 */}
        public OrthogonalGrid(int width, int height)
        {
            Width = width;
            Height = height;
            Columns = new List<Column>(width);
            for (var i = 0; i < width; i++) Columns.Add(new Column(this, i, height));
        }

        public Column this[int i]
        {
            get { return Columns[i]; }
            set { Columns[i] = value; }
        }

        public class Column : Entity
        {
            public int ColumnIndex { get; internal set; }
            public List<Cell> Cells { get; set; }

            public Column() { /* EFcore */ }
            public Column(OrthogonalGrid grid, int columnIndex, int cellCount)
            {
                Cells = new List<Cell>(cellCount);
                ColumnIndex = columnIndex;
                for (var i = 0; i < cellCount; i++) Cells.Add(new Cell(grid, columnIndex, i));
            }

            public Cell this[int i]
            {
                get { return Cells[i]; }
                set { Cells[i] = value; }
            }
        }
    }
}

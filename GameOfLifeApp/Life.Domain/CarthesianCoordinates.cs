﻿namespace Domain
{
    public struct CarthesianCoordinates
    {
        public int X { get; set; }
        public int Y { get; set; }

        public CarthesianCoordinates(int x, int y) : this()
        {
            X = x;
            Y = y;
        }
    }
}

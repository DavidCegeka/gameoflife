﻿using System;
using System.Linq;

namespace Domain
{
    public class Game : Entity
    {
        public short Seed { get; protected set; }
        public int Generation { get; set; }
        public OrthogonalGrid OrthogonalGrid { get; set; }

        public Game() { /* EFCore <3 */}
        public Game(short seed, int width, int height)
        {
            
            var cellCount = width * height;
            var maxSeed = Math.Pow(cellCount, 2);
            if (seed >= maxSeed) throw new ArgumentException($"The provided seed '{seed}' exceeded the maximum seed of '{maxSeed}'!");
            OrthogonalGrid = new OrthogonalGrid(width, height);
            SeedGame(seed);
        }

        public void SeedGame(short seed)
        {
            Seed = seed;
            for (int row = 0; row < OrthogonalGrid.Height; row++)
            {
                for (int cell = 0; cell < OrthogonalGrid.Width; cell++)
                {
                    var isAlive = (Seed >> (int)cell) % 2 == 1;
                    OrthogonalGrid[row][cell] = new Cell(OrthogonalGrid, row, cell, isAlive);
                }
            }
        }

        /// <summary>
        /// Advances the grid by x generations with the following rules : 
        /// Any live cell with fewer than two live neighbors dies, as if by under population.
        /// Any live cell with two or three live neighbors lives on to the next generation.
        /// Any live cell with more than three live neighbors dies, as if by overpopulation.
        /// Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
        /// </summary>
        /// <param name="advanceByGenerations"></param>
        public void AdvanceGeneration(int advanceByGenerations = 1)
        {
            for(var i = 0; i < advanceByGenerations; i++) { 
                var nextGeneration = new OrthogonalGrid(OrthogonalGrid.Width, OrthogonalGrid.Height);
                for (int row = 0; row < OrthogonalGrid.Height; row++)
                {
                    for (int cell = 0; cell < OrthogonalGrid.Width; cell++)
                    {
                        var ancestorCell = OrthogonalGrid[row][cell];
                        var nextGenerationCell = ancestorCell;
                        var livingNeighbouringCells = ancestorCell.NeighbouringCells.Count(x => x.IsAlive);
                        if (ancestorCell.IsAlive && (livingNeighbouringCells < 2 || livingNeighbouringCells > 3)) nextGenerationCell = ancestorCell.Dies(nextGeneration);
                        if (!ancestorCell.IsAlive && livingNeighbouringCells == 3) nextGenerationCell = ancestorCell.IsBorn(nextGeneration);
                        nextGeneration[row][cell] = nextGenerationCell;
                    }
                }
                OrthogonalGrid = nextGeneration;
            }
        }
    }
}